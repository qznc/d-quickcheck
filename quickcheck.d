module quickcheck;

import std.traits;
import std.random;
import std.stdio;
import std.conv: text;

string fillStruct(T)(string name) pure {
	static assert (is(T==struct));
	char[] result;
	string[] members = [ __traits(allMembers,T) ];
	for (uint i=0; i < members.length; i++) {
		string member = members[i];
		result ~= text(name,".",member,"=generate!(typeof(",name, ".",member,"))();\n");
	}
	return result.idup;
}

T generate(T)() {
	static if (isBoolean!(T)) {
		return uniform!"[]"(0,1) == 1;
	} else static if (is(T==float) || is(T==double)) {
		static assert (ulong.sizeof >= T.sizeof);
		union foo { ulong input; T output; }
		foo val = void;
		do {
			val.input = uniform!"[]"(ulong.min, ulong.max);
		} while (val.output == T.infinity
		      || val.output == -T.infinity
		      || val.output != val.output);
		return val.output;
	} else static if (is(T==real)) {
		static assert (ulong.sizeof * 2 == T.sizeof);
		struct twoul { ulong one, two; }
		union foo { twoul input; real output; }
		foo val = void;
		do {
			val.input.one = uniform!"[]"(ulong.min, ulong.max);
			val.input.two = uniform!"[]"(ulong.min, ulong.max);
		} while (val.output == real.infinity
		      || val.output == -real.infinity
		      || val.output != val.output);
		return val.output;
	} else static if (is(T==enum)) {
		auto members = [ EnumMembers!T ];
		ulong i = uniform(0,members.length);
		return members[i];
	} else static if (isIntegral!(T) || isSomeChar!(T)) {
		return uniform!"[]"(T.min,T.max);
	} else static if (isArray!(T)) {
		T result;
		while (uniform(0,100) < 80) {
			result ~= generate!(typeof(result[0]))();
		}
		return result;
	} else static if (is(T==struct)) {
		T result;
		mixin(fillStruct!(T)("result"));
		return result;
	} else {
		static assert (0);
	}
}

unittest {
	bool a   = generate!(bool)();
	float b  = generate!(float)();
	double c = generate!(double)();
	int d    = generate!(int)();
	uint e   = generate!(uint)();
	short f  = generate!(short)();
	ushort g = generate!(ushort)();
	char h   = generate!(char)();
	char[] i = generate!(char[])();
	string j = generate!(string)();
	wchar k  = generate!(wchar)();
	dchar l  = generate!(dchar)();
	byte m   = generate!(byte)();
	ubyte n  = generate!(ubyte)();
}

unittest {
	for (uint i=0; i<100; i++) {
		float x = generate!(float)();
		assert (x != float.infinity);
		assert (x != -float.infinity);
		assert (x == x); // != NaN
		double y = generate!(double)();
		assert (y != double.infinity);
		assert (y != -double.infinity);
		assert (y == y); // != NaN
		real z = generate!(real)();
		assert (z != real.infinity);
		assert (z != -real.infinity);
		assert (z == z); // != NaN
	}
}

unittest {
	T square(T)(T x) { return x*x; }
	for (uint i=0; i<100; i++) {
		int x = generate!(int)();
		assert (square(x) == x*x);
		ulong y = generate!(ulong)();
		assert (square(y) == y*y);
		double z = generate!(double)();
		assert (square(z) == z*z);
	}
}

string mixinCheck(uint arity) {
	char[] result;
	for (uint i=0; i<arity; i++) {
		result ~= text("auto arg",i," = generate!(ParameterTypeTuple!(T)[",i,"])();");
	}
	char[] args;
	for (uint i=0; i<arity; i++) {
		args ~= text("arg",i);
		if (i+1 < arity) { args ~= ","; }
	}
	result ~= text("if (!pred(",args,")) {");
	result ~= text("writeln(text(\"Test failed with arguments \", ",args,"));");
	result ~= text("}");
	return result.idup;
}

void quickcheck(T)(T pred, uint count = 100) {
	static assert (isCallable!(pred));
	static assert (arity!(pred) > 0);
	static assert (isBoolean!(ReturnType!(pred)));
	for (uint i=0; i<count; i++) {
		mixin(mixinCheck(arity!(pred)));
	}
}

unittest {
	bool predicate(uint x) { return x+x == x*2; }
	quickcheck(&predicate);
}

unittest {
	struct foo { int x; float y; }
	bool predicate(foo f) {
		return (f == foo(f.x,f.y));
	}
	quickcheck(&predicate);
}

unittest {
	enum Foo { A, B, C }
	bool predicate(Foo x) { return true; }
	quickcheck(&predicate);
}

int main() {
	return 0;
}
