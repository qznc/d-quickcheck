
default: quickcheck

quickcheck: quickcheck.d
	dmd -w -wi -debug -g -unittest $< -of$@

.PHONY: run test clean

run: quickcheck
	./quickcheck

clean:
	rm -f *.o quickcheck
